﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clonehomework
{
    public  interface IMyCloneable<T> where T : Zerg
    {
        public T MyClone();
    }
}
