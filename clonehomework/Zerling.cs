﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clonehomework
{
    public class Zerling : Larva,ICloneable, IMyCloneable<Zerling>
    {
        public Zerling():base()
        {
        }

        public void Bite()
        {
            Console.WriteLine($"{GetType()}{id} biting");
        }

        public object Clone()
        {
            return MyClone();
        }
        public Zerling MyClone()
        {
            return new Zerling();
        }

       
    }
}
