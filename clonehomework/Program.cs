﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clonehomework
{
    public static class Program
    {
        public static void Main()
        {
            var l1 = new Larva();
            l1.Move();
            Larva l2 = l1.MyClone();
            l2.Move();
            Larva l3 =(Larva) l1.Clone();
            l3.Move();

            var z1 = new Zerling();
            z1.Bite();
            Zerling z2 = z1.MyClone();
            z2.Bite();
            Zerling z3 = (Zerling)z2.Clone();
            z3.Bite();


            var m1 = new Mutalisk();
            m1.Bite();
            m1.Fly();
            Mutalisk m2 = m1.MyClone();
            m2.Fly();
            m2.Bite();
            Mutalisk m3 = (Mutalisk)m1.Clone();
            m3.Fly();
            m3.Bite();

            Console.ReadKey();

        }
    }
}
