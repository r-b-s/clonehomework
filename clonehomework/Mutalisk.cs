﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clonehomework
{
    public  class Mutalisk : Zerling, ICloneable, IMyCloneable<Mutalisk>
    {
        public Mutalisk() : base()
        {
        }

        public void Fly()
        {
            Console.WriteLine($"{GetType()}{id} fliyng");
        }

        public object Clone()
        {
            return MyClone();
        }
        public Mutalisk MyClone()
        {
            return new Mutalisk();
        }
    
    }
}
