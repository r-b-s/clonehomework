﻿namespace clonehomework
{
    public abstract class Zerg
    {
        protected readonly int id;
        static int lastId=0;

        public Zerg()
        {
            id = ++lastId;
        }


        public void Move()
        {
            Console.WriteLine($"{this.GetType()}{id} moving");
        }
    }
}