﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clonehomework
{
    public class Larva : Zerg, IMyCloneable<Larva>, ICloneable
    {
        public Larva():base()
        {
        }

        public  object Clone()
        {
            return MyClone();
        }

        public  Larva MyClone()
        {
            return new Larva();
        }
    }
}
